# Prueba Abb



## Configurar proyecto y lanzar los tests:

```
mkdir prueba_add
git clone https://gitlab.com/LorenaNpm/prueba_abb.git
cd prueba_add
npm install
```

# Lanzar tests en consola

## Tests API:

```
npx cypress run --spec "cypress/integration/API/*"
```

## Tests Search Input:

```
npx cypress run --spec "cypress/integration/UI/search.spec.js"
```

## Tests Login:

```

npx cypress run --spec "cypress/integration/UI/login.spec.js"

```

# Lanzar cypress en entorno gráfico 

```
npx cypress open
```


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

