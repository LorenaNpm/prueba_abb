const login = (user, pass) => {
    if (user) cy.get('#userName').type(user);
    if (pass) cy.get('#password').type(pass);
    cy.get('button[id=login]').click();
};

const loginFromJson = () => {
    cy.fixture('public/public-data.json').then(user => {
        login(user.user, user.pass);
    });
};

describe('Login', () => {

    describe('when the user has just come to the page', () => {
        beforeEach(() => {
            cy.visit('login');
            cy.intercept('POST', '/Account/v1/Login').as('loginPost');
            cy.intercept('POST', '/Account/v1/GenerateToken').as('generateToken');
        });

        it('should go to signup page when the user clicks on the signup button', () => {
            cy.get('#newUser').click();
            cy.url().should('eq', `${Cypress.config().baseUrl}register`);
        });

        it('should login when credentials are correct', () => {
            loginFromJson();
            cy.wait('@loginPost').then(xhr => {
                expect(xhr.response.statusCode).to.equal(200);
                expect(xhr.response.body)
                    .to.have.property('userId')
                    .to.equal('f43275c3-6913-44ec-9664-a1b4b1a632a9');
                expect(xhr.response.body)
                    .to.have.property('username')
                    .to.equal('testtest');
                expect(xhr.response.body)
                    .to.have.property('password')
                    .to.equal('Test123!');
                expect(xhr.response.body)
                    .to.have.property('token')
                    .to.not.be.empty;
            });
            cy.wait('@generateToken').then(xhr => {
                expect(xhr.response.statusCode).to.equal(200);
                expect(xhr.response.body)
                    .to.have.property('token')
                    .not.to.be.empty;
                expect(xhr.response.body)
                    .to.have.property('status')
                    .to.equal('Success');
                expect(xhr.response.body)
                    .to.have.property('result')
                    .to.equal('User authorized successfully.');
            });
        });
        it('should check current url is /profile when credentials are correct', () => {
            loginFromJson();
            cy.url().should('eq', `${Cypress.config().baseUrl}profile`);
        });
        it('should show an invalid credentials notification when the user is empty', () => {
            login(undefined, 'Test123!');
            cy.get('.is-invalid')
                .should('exist')
                .and('be.visible');
        });

        it('should show an invalid credentials notification when the password is empty', () => {
            login('testtest', undefined);
            cy.get('.is-invalid')
                .should('exist')
                .and('be.visible');
        });

        it('should show an invalid credentials notification when the password is not correct', () => {
            login('testtest', 'passfake');
            cy.get('#output')
                .should('exist')
                .and('be.visible')
                .and('have.text', 'Invalid username or password!');
            cy.wait('@generateToken').then(xhr => {
                expect(xhr.response.statusCode).to.equal(200);
                expect(xhr.response.body)
                    .to.have.property('token')
                    .to.be.null;
                expect(xhr.response.body)
                    .to.have.property('status')
                    .to.equal('Failed');
                expect(xhr.response.body)
                    .to.have.property('result')
                    .to.equal('User authorization failed.');
            });
        });
        it('should show an invalid credentials notification when the user is not correct', () => {
            login('userfake', 'Test123!');
            cy.get('#output')
                .should('exist')
                .and('be.visible')
                .and('have.text', 'Invalid username or password!');
            cy.wait('@generateToken').then(xhr => {
                expect(xhr.response.statusCode).to.equal(200);
                expect(xhr.response.body)
                    .to.have.property('token')
                    .to.be.null;
                expect(xhr.response.body)
                    .to.have.property('status')
                    .to.equal('Failed');
                expect(xhr.response.body)
                    .to.have.property('result')
                    .to.equal('User authorization failed.');
            });
        });
    });
});
