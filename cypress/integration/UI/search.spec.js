import 'cypress-react-selector';

describe('Books Search', () => {
    beforeEach(() => {
        cy.clearCookies();
        cy.visit(`books`);
        cy.waitForReact();
    });

    it('should validate total number of Books', () => {
        cy.get('.mr-2').its('length').then((val) => {
            cy.log(' Total visible books >> ' + val);
            expect(val).to.be.a('number');
            expect(val).to.be.at.least(8);
        });
    });

    it('should check total items listed before searching', () => {
        cy.get('.mr-2').its('length').should('be.at.least', 8)
    });
    it('should type character by character to check elements are changing', () => {
        cy.get('#searchBox').type('g').then(() => {
            cy.get('.mr-2').its('length').should('be.lessThan', 8)
        });
        cy.get('#searchBox').type('i').then(() => {
            cy.get('.mr-2').its('length').should('be.lessThan', 6)
        });
        cy.get('#searchBox').type('t').then(() => {
            cy.get('.mr-2').its('length').should('be.at.least', 1)
        });
    });

    it('should check list is empty when there is no coincidences', () => {
        cy.get('#searchBox').type('gitt').then(() => {
            cy.get('.mr-2').should('not.exist');
        });
    });

    it('should check main React components', () => {
        cy.getReact('Tbody').should('exist');
        cy.getReact('TrGroupComponent').should('exist').should('have.length.gt', 0);
        cy.getReact('TrComponent').should('exist').should('have.length.gt', 0);
        cy.getReact('TdComponent').should('exist').should('have.length.gt', 0);
        cy.getReact('Cell').should('exist').should('have.length.gt', 0);
        cy.getReact('FormControl').nthNode(0)
            .getProps('id')
            .should('eq', 'searchBox');
        cy.getReact('Thead').nthNode(0)
            .getProps('className')
            .should('eq', "-header");
    });
});