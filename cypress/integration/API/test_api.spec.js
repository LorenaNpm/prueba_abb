describe('API tests - ABB', () => {
    it('should test BookStore/v1/Books call ', () => {
        cy.fixture('public/public-data.json').then(function (item) {
            expect(item).to.be.a('object');
            cy.request({
                method: 'GET',
                url: `${item.urlApi}BookStore/v1/Books`,
                failOnStatusCode: false,
                log: true
            }).then(xhr => {
                expect(xhr).to.be.a('object');
                expect(xhr.status).to.equal(200);
                expect(xhr.body.books).to.have.length(8);
                expect(xhr.body.books).to.be.a('array');

                xhr.body.books.forEach(element => {
                    expect(element).to.have.property('isbn').to.be.a("string").not.to.be.empty;
                    expect(element).to.have.property('title').to.be.a("string").not.to.be.empty;
                    expect(element).to.have.property('subTitle').to.be.a("string");
                    expect(element).to.have.property('author').to.be.a("string").not.to.be.empty;
                    expect(element).to.have.property('publish_date').to.be.a("string");
                    expect(element).to.have.property('publisher').to.be.a("string");
                    expect(element).to.have.property('pages').to.be.a("number").not.to.be.null;
                    expect(element).to.have.property('description').to.be.a("string");
                    expect(element).to.have.property('website').to.be.a("string");

                    cy.log(' ISBN: ' + element.isbn);
                    cy.log(' Title: ' + element.title);
                    cy.log(' SubTitle: ' + element.subTitle);
                    cy.log(' Author: ' + element.author);
                    cy.log(' Publish date: ' + element.publish_date);
                    cy.log(' Pages: ' + element.pages);
                    cy.log(' Description: ' + element.description);
                    cy.log(' Website: ' + element.website);
                });
            });
        });
    });
});
